package com.mobstac.onboarding;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.UUID;

import com.mobstac.beaconstac.Beaconstac;
import com.mobstac.beaconstac.core.MSException;
import com.mobstac.beaconstac.interfaces.MSErrorListener;
import com.mobstac.beaconstac.interfaces.BeaconScannerCallbacks;
import com.mobstac.beaconstac.interfaces.MSSyncListener;
import com.mobstac.beaconstac.models.MBeacon;
import com.mobstac.beaconstac.models.MRule;
import com.mobstac.beaconstac.utils.MSConstants;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeAction();

        try{
            getCallBack();
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }

        ToggleButton toggle = findViewById(R.id.toggleButton);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    startToggle();
                } else {
                    // The toggle is disabled
                    stopToggle();
                }
            }
        });

        final TextView resultView = findViewById(R.id.textView4);
        final EditText lastName   = findViewById(R.id.editText);
        final EditText pnrNum = findViewById(R.id.editText2);
        final Button submit = findViewById(R.id.button2);

        Beaconstac.getInstance().setBeaconScannerCallbacks(new BeaconScannerCallbacks() {

            @Override
            public void onCampedBeacon(MBeacon beacon) {
                System.out.println("On Camped Beacon Triggered");
                String beaconsno = beacon.getSerialNumber();
                int beaconid = beacon.getId();
                int beaconpid = beacon.getPlaceId();

                resultView.append("Beacon Serial : " + beaconsno + "\n");
                resultView.append("Beacon ID : " + Integer.toString(beaconid) + "\n");
                resultView.append("Beacon Place ID : " + Integer.toString(beaconpid) + "\n");
            }

            @Override
            public void onScannedBeacons(ArrayList<MBeacon> rangedBeacons) {
            }

            @Override
            public void onExitedBeacon(MBeacon beacon) {
            }

            @Override
            public void onRuleTriggered(MRule rule) {
            }


        });

        submit.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View view)
                    {
                        //Log.v("EditText", mEdit.getText().toString());
                        resultView.append("Last Name : " + lastName.getText().toString() + "\n");
                        resultView.append("PNR Num: " + pnrNum.getText().toString() + "\n");
                    }
                    // Pass params to DataBase
                });
        /*

        lastName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    resultView.append(lastName.getText().toString());
                    System.out.println(lastName.getText().toString());
                    handled = true;
                }
                return handled;
            }
        });

        pnrNum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    resultView.append(pnrNum.getText().toString());
                    System.out.println(pnrNum.getText().toString());
                    handled = true;
                }
                return handled;
            }
        });
        */

        // Call database function here
        /*
        DbConnection db = new DbConnection();
        db.generatePass(lastName.getText().toString(), pnrNum.getText().toString(), resultView.getContext());
        */

    }
    private void initializeAction() {
        try {
            Beaconstac.initialize(getApplicationContext(), "e62435a78e67ec98bba3b879ba00448650032557", new MSSyncListener() {

                @Override
                public void onSuccess() {
                    Log.d("Beaconstac", "Initialization successful");
                    Beaconstac.getInstance().startScanningBeacons(new MSErrorListener() {
                        @Override
                        public void onError(MSException msException) {

                        }
                    });
                }

                @Override
                public void onFailure(MSException e) {
                    Log.d("Beaconstac", "Initialization failed");
                }

            });
        }
        catch (MSException | SecurityException e) {
            e.printStackTrace();
        }
        System.out.println("Initialized");
    }

    private void stopToggle(){
        try
        {
            Beaconstac.getInstance().stopScanningBeacons(new MSErrorListener() {
                @Override
                public void onError(MSException e) {
                    e.printStackTrace();
                }
            });
            System.out.println("Stopped Scanning");
        }
            catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    private void startToggle(){
        try {
            Beaconstac.getInstance().startScanningBeacons(new MSErrorListener() {
                @Override
                public void onError(MSException e) {
                    e.printStackTrace();
                }
            });
            System.out.println("Started Scanning");
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
    }
    private void getCallBack(){

    }
}

